## Descripción

Esta es una API REST elaborada para la gestión de usuarios y sus tareas, para su ejecución por favor seguir los siguientes pasos.

## Base de datos

En este caso se uso PostgreSQL como motor de base de datos, se deja aqui un archivo llamado `docker-compose.yml` para que lo use en el caso de que quiera crear una instancia de base de datos postgres con las credenciales del archivo de ejemplo de las variables de entorno para su comodidad.

## Variables de entorno

Debe agregar un archivo llamado `.development.env` el cual debe tener la estructura del archivo `.copy.env`, ahi por favor reemplazar los datos relacionados a la base de datos y los de email para que pueda recibir correos de validación de usuario, esto le permitira realizar una correcta autorización de los usuarios que cree.

## Installation

```bash
$ npm install
```

## Running the app

Por favor ejecutar uno de los siguientes comandos y posteriormente en caso de que allá conservado el puerto dejado por defecto en su navegador podra hacer uso de la api rest desarrollada en este proyecto

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
