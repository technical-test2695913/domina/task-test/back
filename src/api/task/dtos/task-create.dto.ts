import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty, IsOptional
} from 'class-validator';

// Entties
import { User } from 'src/api/user/entities';

export class TaskCreateDto {
  @ApiProperty({ example: 'Test task', description: 'The text of the task' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  text: string;

  @ApiProperty({ example: 1, description: 'The user of the task' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  user: User;

  @ApiProperty({ example: 'es', description: 'The language of the task' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  lang: string;
}
