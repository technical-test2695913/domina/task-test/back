import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class TaskEditDto {
  @ApiProperty({ example: 'Test task', description: 'The text of the task' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  text: string;

  @ApiProperty({ example: true, description: 'The state of the task' })
  @IsOptional()
  check: boolean;
}
