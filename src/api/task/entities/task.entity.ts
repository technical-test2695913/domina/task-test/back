import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  JoinColumn,
  UpdateDateColumn
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

// Relations
import { User } from 'src/api/user/entities';

@Entity('tasks', {
  orderBy: {
    createdAt: 'DESC',
  },
})
export class Task {
  @ApiProperty({ example: 1, description: 'Id of the task' })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 500, nullable: true })
  text: string;

  @ManyToOne(() => User, (user) => user.tasks, { eager: true })
  @JoinColumn({ name: 'user' })
  user: User;

  @Column({ type: 'bool', default: false, nullable: true })
  check: boolean;

  @Column({ type: 'bool', default: true, nullable: true })
  active: boolean;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt: Date;
}
