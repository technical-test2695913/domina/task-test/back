import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  UseGuards,
  Headers,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

// Services
import { TaskService } from './task.service';

// Data transfer objects
import { TaskCreateDto, TaskEditDto } from './dtos';

// Guards
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiTags('Task routes')
@ApiBearerAuth('defaultBearerAuth')
@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) { }

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: 'List task' })
  async getMany() {
    const data = await this.taskService.getMany();
    return { data };
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @ApiOperation({ summary: 'Get task by id' })
  async getOne(@Param('id') id: number) {
    const data = await this.taskService.getOne(id);
    return { data };
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOperation({ summary: 'Create task' })
  async create(@Body() dto: TaskCreateDto, @Headers() headers: any) {
    const data = await this.taskService.create(dto);
    return { message: 'TASK_CREATED', data };
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  @ApiOperation({ summary: 'Update task by id (Any task atributes)' })
  async edit(@Param('id') id: number, @Body() dto: TaskEditDto) {
    return {
      message: 'TASK_EDITED',
      data: !!(await this.taskService.edit(id, dto)).affected,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  @ApiOperation({ summary: 'Delete task' })
  async deleteOne(@Param('id') id: number) {
    return {
      message: 'TASK_DELETED',
      data: !!(await this.taskService.delete(id)).affected,
    };
  }
}
