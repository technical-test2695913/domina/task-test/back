import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

// Services
import { UserService } from '../user/user.service';

// Entities
import { Task } from './entities';

// Data transfer objects
import { TaskCreateDto, TaskEditDto } from './dtos';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private readonly postRepository: Repository<Task>,
    private readonly userService: UserService
  ) { }

  async getMany() {
    return await this.postRepository.find({
      where: { active: true },
      relations: ['user']
    });
  }

  async getOne(id: number, errorMsg = 'VAL_TASK_NOT_EXIST_OR_UNAUTHORIZED') {
    const post = await this.postRepository.findOne({
      where: { id },
      loadRelationIds: true,
    });

    if (!post) throw new NotFoundException(errorMsg);

    return post;
  }

  async create(dto: TaskCreateDto) {
    await this.userService.getOne(
      dto.user?.id,
      'VAL_USER_NOT_EXIST',
    );
    if (dto.text.length < 10) throw new NotFoundException('VAL_TASK_MIN');
    if (dto.text.length > 500) throw new NotFoundException('VAL_TASK_MAX');

    const newPost = this.postRepository.create(dto);
    return await this.postRepository.save(newPost);
  }

  async edit(id: number, dto: TaskEditDto) {
    await this.getOne(id, 'VAL_TASK_NOT_EXIST');

    return await this.postRepository.update(id, dto);
  }

  async delete(id: number) {
    return await this.edit(id, { active: false } as any);
  }
}
