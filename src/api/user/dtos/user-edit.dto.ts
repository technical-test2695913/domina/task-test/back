import { PartialType } from '@nestjs/mapped-types';
import { ApiPropertyOptional } from '@nestjs/swagger';

// Related
import { UserCreateDto } from './user-create.dto';

export class UserEditDto extends PartialType(UserCreateDto) {
  @ApiPropertyOptional({ example: true, description: 'The state of the user' })
  active: boolean;

  @ApiPropertyOptional({
    example: true,
    description: 'The verify state of the user',
  })
  verified: boolean;
}
