import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  BeforeInsert,
  BeforeUpdate,
  Entity,
  UpdateDateColumn,
  OneToOne,
} from 'typeorm';
import { hash } from 'bcryptjs';
import { ApiProperty } from '@nestjs/swagger';
import { Length } from 'class-validator';

// Types
import { Gender } from 'src/types/Gender';

// Entities
import { Task } from 'src/api/task/entities';

@Entity('users', {
  orderBy: {
    createdAt: 'DESC',
  },
})
export class User {
  @ApiProperty({ example: 1, description: 'Id of the user' })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  @Length(3, 255, { message: 'VAL_MIN_MAX|3|255' })
  name: string;

  @Column({ name: 'last_name', type: 'varchar', length: 255 })
  @Length(3, 255, { message: 'VAL_MIN_MAX|3|255' })
  lastName: string;

  @Column({ name: 'birth_day', type: 'date' })
  birthDay: Date;

  @Column({
    type: 'enum',
    enum: Gender,
    default: Gender.O
  })
  gender: Gender

  @Column({ type: 'varchar', length: 200, nullable: true })
  @Length(3, 200, { message: 'VAL_MIN_MAX|3|200' })
  country: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  @Length(3, 200, { message: 'VAL_MIN_MAX|3|200' })
  city: string;

  @Column({ type: 'varchar', length: 500, nullable: true })
  @Length(3, 500, { message: 'VAL_MIN_MAX|3|500' })
  address: string;

  @Column({ type: 'varchar', length: 255, unique: true })
  @Length(3, 255, { message: 'VAL_MIN_MAX|8|255' })
  email: string;

  @Column({ type: 'varchar', length: 128, select: false })
  @Length(8, 128, { message: 'VAL_MIN_MAX|6|128' })
  password: string;

  @Column({ type: 'bool', default: true, nullable: true })
  active: boolean;

  @Column({ type: 'bool', default: false, nullable: true })
  verified: boolean;

  @Column({ type: 'bool', default: false, nullable: true })
  approved: boolean;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (!this.password) {
      return;
    }
    this.password = await hash(this.password, 10);
  }

  @OneToOne(() => Task, (post) => post.user, { cascade: true })
  tasks: Task;
}
