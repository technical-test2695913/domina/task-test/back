import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  UseGuards,
  Headers,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

// Services
import { UserService } from './user.service';

// Data transfer objects
import { UserCreateDto, UserEditDto } from './dtos';

// Guards
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

// Utils
import { globals } from 'src/utils/globals';

@ApiTags('Users routes')
@ApiBearerAuth('defaultBearerAuth')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: 'List users' })
  async getMany() {
    const data = await this.userService.getMany();
    return { data };
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @ApiOperation({ summary: 'Get user by id' })
  async getOne(@Param('id') id: number) {
    const data = await this.userService.getOne(id);
    return { data };
  }

  @UseGuards(JwtAuthGuard)
  @Get('email/:email')
  @ApiOperation({ summary: 'Get one by email' })
  async getOneByCode(@Param('email') email: string) {
    const data = await this.userService.getOneByEmail(email);
    return { data };
  }

  @Post()
  @ApiOperation({ summary: 'Create user' })
  async create(@Body() dto: UserCreateDto, @Headers() headers: any) {
    globals.appURL = headers.origin;
    const data = await this.userService.create(dto);
    return { message: 'USER_CREATED', data };
  }

  @Post('verify/:token')
  @ApiOperation({ summary: 'Verify user' })
  async verify(@Param('token') token: string) {
    return {
      message: 'USER_VERIFIED',
      data: !!(await this.userService.verify(token)).affected,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  @ApiOperation({ summary: 'Update user by id (Any user atributes)' })
  async edit(@Param('id') id: number, @Body() dto: any) {
    return {
      message: 'USER_EDITED',
      data: !!(await this.userService.edit(id, dto)).affected,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  @ApiOperation({ summary: 'Delete user' })
  async deleteOne(@Param('id') id: number) {
    return {
      message: 'USER_DELETED',
      data: !!(await this.userService.delete(id)).affected,
    };
  }
}
