import { Injectable, NotFoundException } from '@nestjs/common';
import { Not, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';


// Entities
import { User } from './entities';

// Data transfer objects
import { UserCreateDto, UserEditDto } from './dtos';

// Utils
import { sendEmail } from 'src/utils/mailer';
import { getToken, verifyToken } from 'src/utils/jwt';
import { globals } from 'src/utils/globals';

// Translations
import { es } from 'src/utils/mailer/templates/create/es';
import { en } from 'src/utils/mailer/templates/create/en';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) { }

  async getMany() {
    return await this.userRepository.find({
      where: { active: true },
      select: {
        id: true,
        name: true,
        lastName: true,
        email: true,
        city: true,
        createdAt: true,
      }
    });
  }

  async getOne(id: number, errorMsg = 'VAL_USER_NOT_EXIST_OR_UNAUTHORIZED') {
    const user = await this.userRepository.findOne({
      where: { id },
      loadRelationIds: true,
    });

    if (!user) throw new NotFoundException(errorMsg);

    return user;
  }

  async getOneByEmail(
    email: string,
    errorMsg = 'VAL_USER_NOT_EXIST_OR_UNAUTHORIZED',
  ) {
    const user = await this.userRepository.findOne({
      where: { email },
      loadRelationIds: true,
    });

    if (!user) throw new NotFoundException(errorMsg);

    return user;
  }

  async getAuthByEmail(
    email: string,
    errorMsg = 'VAL_USER_NOT_EXIST_OR_UNAUTHORIZED',
  ) {
    const user = await this.userRepository.findOne({
      where: { email },
      select: {
        id: true,
        email: true,
        password: true,
        active: true,
        verified: true,
        createdAt: true,
      },
      loadEagerRelations: false,
    });

    if (!user) throw new NotFoundException(errorMsg);

    return user;
  }

  async create(dto: UserCreateDto) {
    if (await this.userRepository.findOneBy({ email: dto.email }))
      throw new NotFoundException('VAL_USER_ALREADY_REGISTERED_WITH_EMAIL');

    const newUser = this.userRepository.create(dto);
    const user = await this.userRepository.save(newUser);

    await sendEmail(
      [dto.email],
      [
        [dto.name, dto.lastName].join(' '),
        dto.email,
        `${globals.appURL}/verify-user?userVerify=${getToken({
          sub: user.id,
          email: dto.email,
        })}`,
      ],
      dto.lang === 'es' ? es : en,
    );

    delete user.password;
    return user;
  }

  async verify(token: string) {
    const payload = verifyToken(token);
    if (!payload) throw new NotFoundException('VAL_TOKEN_NOT_FOUND');

    return this.edit(payload.sub, { verified: true } as any);
  }

  async edit(id: number, dto: UserEditDto) {
    await this.getOne(id, 'VAL_USER_NOT_EXIST');
    if (
      dto.email &&
      (await this.userRepository.findOneBy({ email: dto.email, id: Not(id) }))
    )
      throw new NotFoundException(
        'VAL_OTHER_USER_ALREADY_REGISTERED_WITH_EMAIL',
      );

    console.log(dto)
    return await this.userRepository.update(id, dto);
  }

  async delete(id: number) {
    return await this.edit(id, { active: false } as any);
  }
}
