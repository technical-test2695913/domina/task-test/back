import {
  INestApplication,
  NotFoundException,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';

export const appClassValidator = (app: INestApplication) =>
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      exceptionFactory: (errors: ValidationError[]) => {
        const res = {};
        errors.forEach((e) => (res[e.property] = Object.values(e.constraints)));
        throw new NotFoundException({
          statusCode: 404,
          validationsError: res,
        });
      },
    }),
  );
