import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const appSwagger = (app: INestApplication) => {
  const config = new DocumentBuilder()
    .setTitle('Domina - tasks')
    .setDescription('API para la Domina - tasks.')
    .setVersion('1.0.0')
    .addBearerAuth(undefined, 'defaultBearerAuth')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
};
