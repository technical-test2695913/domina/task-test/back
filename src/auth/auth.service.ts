/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare, hash } from 'bcryptjs';

// Services
import { UserService } from 'src/api/user/user.service';

// Types
import { User } from 'src/api/user/entities';

// DTOs
import { ChangePassApplyDto, ChangePassDto } from './dtos/change-pass.dto';

// Utils
import { sendEmail } from 'src/utils/mailer';
import { globals } from 'src/utils/globals';

// Translations
import { es } from 'src/utils/mailer/templates/change-pass/es';
import { en } from 'src/utils/mailer/templates/change-pass/en';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    public jwtService: JwtService,
  ) { }

  async validateUser(username: string, pass: string): Promise<User> {
    const user = await this.userService.getAuthByEmail(username);
    if (user && (await compare(pass, user.password))) {
      delete user.password;
      return user;
    }
    return null;
  }

  async login(user: User) {
    if (!user.verified) throw new NotFoundException('VAL_USER_NOT_VERIFIED');
    const payload = { userName: user.email, sub: (user as any).id };
    return {
      data: {
        access_token: this.jwtService.sign(payload),
        id: user.id,
        email: user.email
      },
    };
  }

  async changePassApply(data: ChangePassApplyDto) {
    const user = await this.userService.getAuthByEmail(
      data.email,
      'VAL_USER_NOT_EXIST_WITH_THIS_EMAIL',
    );

    const payload = { userName: user.email, sub: (user as any).id };

    return sendEmail(
      [user.email],
      [
        `${globals.appURL}/change-pass?changePassToken=${this.jwtService.sign(
          payload,
          {
            expiresIn: '1h',
          },
        )}`,
      ],
      data.lang === 'es' ? es : en,
    );
  }

  async changePass(data: ChangePassDto) {
    if (data.password !== data.confirmPassword)
      throw new NotFoundException('VAL_PASS_NOT_MATCH');

    try {
      const payload: any = this.jwtService.verify(data.changePassToken);
      const dto: any = { password: await hash(data.password, 10) };

      return await this.userService.edit(payload.sub, dto);
    } catch (error) {
      throw new NotFoundException('VAL_TOKEN_NOT_FOUND');
    }
  }
}
