import { NestFactory } from '@nestjs/core';
import * as bodyParser from 'body-parser';

// App Module
import { AppModule } from './app.module';

// Configurations
import { appSwagger } from './app.swagger';
import { appClassValidator } from './app.class-validator';

// Utils
import { logger } from './utils/logger';
import { globals } from './utils/globals';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.setGlobalPrefix('api');

  appClassValidator(app);
  appSwagger(app);

  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

  await app.listen(process.env.PORT || 3000);
  globals.appURL = await app.getUrl();
  logger(`Application is running on: ${globals.appURL}`);
  logger(`Swagger is running on: ${await app.getUrl()}/api`);
}

bootstrap();
