export interface EmailContent {
  subject: string;
  html: string;
}
