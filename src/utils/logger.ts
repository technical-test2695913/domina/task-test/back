import { Logger } from '@nestjs/common';

export const logger = async (msg: string, type = 'log') =>
  new Logger()[type](msg);
