import { NotFoundException } from '@nestjs/common';

// Connections
import { connEthereal } from './connections/ethereal';
import { connGmail } from './connections/gmail';

// Interfaces
import { EmailContent } from 'src/types/email-content';

export const sendEmail = async (
  to: string[],
  values: string[],
  content: EmailContent,
  type: 'gmail' | 'ethereal' = 'gmail',
) => {
  try {
    const transporter =
      type === 'gmail' ? await connGmail() : await connEthereal();

    const info = await transporter.sendMail({
      from: `"Domina - Tasks" <${(transporter.options as any).auth.user
        }>`,
      to: to.join(','),
      subject: content.subject,
      html: values.reduce(
        (old, value, index) => old.replace(`arg${index}`, value),
        content.html,
      ),
    });

    return info;
  } catch (error) {
    throw new NotFoundException('VAL_EMAIL_NOT_SENT');
  }
};
